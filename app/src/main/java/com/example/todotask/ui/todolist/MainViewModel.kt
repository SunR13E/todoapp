package com.example.todotask.ui.todolist

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.todotask.domain.DeleteTaskUseCase
import com.example.todotask.domain.GetTaskListUseCase
import com.example.todotask.domain.UpdateTaskUseCase
import com.example.todotask.entity.ToDoItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainViewModel(
    private val getTaskListUseCase: GetTaskListUseCase,
    private val updateTaskUseCase: UpdateTaskUseCase,
    private val deleteTaskUseCase: DeleteTaskUseCase
) : ViewModel() {

    val toDoList: MutableLiveData<List<ToDoItem>> = MutableLiveData()
    val showTaskMenu: MutableLiveData<Int> = MutableLiveData()
    private val compositeDisposable = CompositeDisposable()

    fun loadTasks() {
        val list = getTaskListUseCase.execute()
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    toDoList.value = it.toList()
                },
                { Log.e("MainViewModel:", it.message.orEmpty()) }
            )
        compositeDisposable.add(list)
    }


    fun settings(position: Int) {
        val task = toDoList.value?.get(position)
        showTaskMenu.value = task?.taskNumber
    }

    fun pending(position: Int) {
        val task = toDoList.value?.get(position)
        if (task != null) {
            val pending = updateTaskUseCase.execute(ToDoItem(task.taskNumber, task.taskName, 0))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { Log.d("SUCCESS", "success pending") },
                    { Log.e("ERROR", "error pending") }
                )

            compositeDisposable.add(pending)
        }
        Log.d("PENDING", "pending")
        loadTasks()
    }

    fun urgent(position: Int) {
        val task = toDoList.value?.get(position)
        if (task != null) {
            val urgent = updateTaskUseCase.execute(ToDoItem(task.taskNumber, task.taskName, 2))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { Log.d("SUCCESS", "success urgent") },
                    { Log.e("ERROR", "error urgent") }
                )

            compositeDisposable.add(urgent)

        }
        Log.d("URGENT", "urgent")
        loadTasks()
    }

    fun delete(position: Int) {
        val task = toDoList.value?.get(position)
        if (task != null) {
            val remove = deleteTaskUseCase.execute(task)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { Log.d("SUCCESS", "success delete") },
                    { Log.e("ERROR", "error delete") }
                )

            compositeDisposable.add(remove)
        }
        Log.d("DELETE", "deleted")
        loadTasks()
    }

    fun progress(position: Int) {
        val task = toDoList.value?.get(position)
        if (task != null) {
            val pending = updateTaskUseCase.execute(ToDoItem(task.taskNumber, task.taskName, 3))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { Log.d("SUCCESS", "success progress") },
                    { Log.e("ERROR", "error progress") }
                )

            compositeDisposable.add(pending)
        }
        Log.d("PENDING", "pending")
        loadTasks()
    }

}