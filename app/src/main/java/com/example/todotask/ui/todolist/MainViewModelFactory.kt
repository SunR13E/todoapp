package com.example.todotask.ui.todolist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.todotask.domain.DeleteTaskUseCase
import com.example.todotask.domain.GetTaskListUseCase
import com.example.todotask.domain.UpdateTaskUseCase

@Suppress("UNCHECKED_CAST")
class MainViewModelFactory(
    private val deleteUseCase: DeleteTaskUseCase,
    private val getListUseCase: GetTaskListUseCase,
    private val updateUseCase: UpdateTaskUseCase
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(
                deleteTaskUseCase = deleteUseCase,
                getTaskListUseCase = getListUseCase,
                updateTaskUseCase = updateUseCase
            ) as T
        }
        throw IllegalArgumentException("Unknown class")
    }
}