package com.example.todotask.ui.todolist

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.todotask.*
import com.example.todotask.entity.ToDoListDatabase
import com.example.todotask.domain.DeleteTaskUseCase
import com.example.todotask.domain.GetTaskListUseCase
import com.example.todotask.domain.UpdateTaskUseCase
import com.example.todotask.entity.ToDoListRepository

class MainFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_to_do_list, container, false)
    }

    lateinit var viewModelFactory: MainViewModelFactory
    lateinit var viewModel: MainViewModel
    lateinit var adapter: ToDoListAdapter

    override fun onResume() {
        super.onResume()
        viewModel.loadTasks()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val database = ToDoListDatabase.getDatabase(requireContext())
        val repository = ToDoListRepository(database.toDoListDao())
        val updateUseCase = UpdateTaskUseCase(repository)
        val deleteUseCase = DeleteTaskUseCase(repository)
        val getListUseCase = GetTaskListUseCase(repository)
        val toDoList = view.findViewById<RecyclerView>(R.id.todo_list)
        viewModelFactory = MainViewModelFactory(
            updateUseCase = updateUseCase,
            deleteUseCase = deleteUseCase,
            getListUseCase = getListUseCase
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)
        viewModel.loadTasks()

        view.findViewById<TextView>(R.id.add_task).setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
        }

        adapter = ToDoListAdapter(
            { viewModel.settings(it) },
            { viewModel.pending(it) },
            { viewModel.urgent(it) },
            { viewModel.delete(it) },
            { viewModel.progress(it) })

        viewModel.toDoList.observe(viewLifecycleOwner, { adapter.addData(it) })
        toDoList.adapter = adapter
        toDoList.layoutManager = LinearLayoutManager(requireContext())
        viewModel.showTaskMenu.observe(viewLifecycleOwner, {})

    }
}