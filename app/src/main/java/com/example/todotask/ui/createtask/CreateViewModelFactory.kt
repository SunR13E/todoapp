package com.example.todotask.ui.createtask

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.todotask.domain.AddTaskUseCase

@Suppress("UNCHECKED_CAST")
class CreateViewModelFactory(private val useCase: AddTaskUseCase) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CreateTaskViewModel::class.java)) {
            return CreateTaskViewModel(useCase) as T
        }
        throw IllegalArgumentException("Unknown class")
    }
}