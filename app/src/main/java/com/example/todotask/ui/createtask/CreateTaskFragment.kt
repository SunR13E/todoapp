package com.example.todotask.ui.createtask

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.todotask.domain.AddTaskUseCase
import com.example.todotask.R
import com.example.todotask.entity.ToDoListDatabase
import com.example.todotask.entity.ToDoListRepository
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class CreateTaskFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_create_task, container, false)
    }

    lateinit var viewModelFactory: CreateViewModelFactory
    lateinit var viewModel: CreateTaskViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val database = ToDoListDatabase.getDatabase(requireContext())
        val repository = ToDoListRepository(database.toDoListDao())
        val useCase = AddTaskUseCase(repository)
        viewModelFactory = CreateViewModelFactory(useCase)
        viewModel = ViewModelProvider(this, viewModelFactory).get(CreateTaskViewModel::class.java)
        val taskText = view.findViewById<TextInputEditText>(R.id.task)
        val taskTextContainer = view.findViewById<TextInputLayout>(R.id.task_container)
        val importantChecker = view.findViewById<CheckBox>(R.id.important)

        taskText.addTextChangedListener { taskTextContainer.error = null }
        viewModel.error.observe(viewLifecycleOwner, { isEnabled ->
            if (isEnabled) {
                taskTextContainer.error = getString(R.string.error)
            } else {
                taskTextContainer.error = null
            }
        })

        view.findViewById<TextView>(R.id.back).setOnClickListener {
            findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
        }

        view.findViewById<TextView>(R.id.set).setOnClickListener {
            viewModel.addTask(taskText.text.toString(), importantChecker.isChecked)
            if (taskText.text.toString().isNotBlank()) {
                findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
            }
        }
    }
}