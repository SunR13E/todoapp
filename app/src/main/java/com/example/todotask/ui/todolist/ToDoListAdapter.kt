package com.example.todotask.ui.todolist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.todotask.R
import com.example.todotask.ui.todolist.ToDoListAdapter.ItemViewHolder
import com.example.todotask.entity.ToDoItem

class ToDoListAdapter(
    private val viewOnClickListener: (Int) -> Unit,
    private val pendingOnClickListener: (Int) -> Unit,
    private val urgentOnClickListener: (Int) -> Unit,
    private val removeOnClickListener: (Int) -> Unit,
    private val progressOnClickListener: (Int) -> Unit,
) : RecyclerView.Adapter<ItemViewHolder>() {

    private val tasks: MutableList<ToDoItem> = mutableListOf()

    class ItemViewHolder(
        view: View,
        private val viewOnClickListener: (Int) -> Unit,
        private val pendingOnClickListener: (Int) -> Unit,
        private val urgentOnClickListener: (Int) -> Unit,
        private val removeOnClickListener: (Int) -> Unit,
        private val progressOnClickListener: (Int) -> Unit
    ) : RecyclerView.ViewHolder(
        view
    ) {
        private val progressText = view.findViewById<TextView>(R.id.task_in_progress_text)
        private val settingContainer =
            itemView.findViewById<ConstraintLayout>(R.id.setting_container)
        private val itemBackground: ConstraintLayout =
            itemView.findViewById(R.id.recycler_view_item) as ConstraintLayout
        private val textTask = view.findViewById(R.id.to_do_task) as TextView

        init {
            settingContainer.setOnClickListener { popupMenu(itemView.context, settingContainer) }
        }

        fun bind(task: ToDoItem) {

            popupMenu(itemView.context, settingContainer)

            if (task.taskType == 2) {
                itemBackground.setBackgroundResource(R.drawable.to_do_urgently_background)
                progressText.visibility = View.INVISIBLE
            }
            if (task.taskType == 0) {
                itemBackground.setBackgroundResource(R.drawable.to_do_pending_background)
                progressText.visibility = View.INVISIBLE
            }
            if (task.taskType == 1) {
                itemBackground.setBackgroundResource(R.drawable.to_do_item_background)
                progressText.visibility = View.INVISIBLE
            }
            if (task.taskType == 3) {
                itemBackground.setBackgroundResource(R.drawable.to_do_urgently_background)
                progressText.visibility = View.VISIBLE
            }
            textTask.text = task.taskName

        }

        private fun popupMenu(context: Context, view: ConstraintLayout) {
            val popupMenu = PopupMenu(context, view)
            popupMenu.inflate(R.menu.menu_main)
            popupMenu.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.pending -> {
                        pendingOnClickListener(adapterPosition)
                        true
                    }
                    R.id.urgently -> {
                        urgentOnClickListener(adapterPosition)
                        true
                    }
                    R.id.remove -> {
                        removeOnClickListener(adapterPosition)
                        true
                    }
                    R.id.taskInProgress -> {
                        progressOnClickListener(adapterPosition)
                        true
                    }
                    else -> false
                }
            }

            view.setOnClickListener {
                popupMenu.show()
            }
        }
    }

    fun addData(response: List<ToDoItem>) {
        tasks.clear()
        tasks.addAll(response)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.to_do_list_item, parent, false)
        return ItemViewHolder(
            view,
            viewOnClickListener,
            pendingOnClickListener,
            urgentOnClickListener,
            removeOnClickListener,
            progressOnClickListener
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val task = tasks[position]
        holder.bind(task)

    }

    override fun getItemCount(): Int {
        return tasks.size
    }
}