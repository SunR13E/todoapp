package com.example.todotask.ui.createtask

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.todotask.domain.AddTaskUseCase
import com.example.todotask.entity.ToDoItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class CreateTaskViewModel(
    private val addTaskUseCase: AddTaskUseCase
) : ViewModel() {

    val error: MutableLiveData<Boolean> = MutableLiveData()
    private val compositeDisposable = CompositeDisposable()

    fun addTask(task: String, taskChecked: Boolean) {
        var taskType = 1

        if (task.isBlank()) {
            error.value = true
            return
        }

        if (taskChecked) {
            taskType = 2
        }

        val disposable =
            addTaskUseCase.execute(ToDoItem(taskName = task, taskType = taskType))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { Log.d("SUCCESS", "success added") },
                    { Log.e("ERROR", "error added") }
                )

        compositeDisposable.add(disposable)

    }
}