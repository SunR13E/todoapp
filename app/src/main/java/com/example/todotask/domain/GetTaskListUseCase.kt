package com.example.todotask.domain

import com.example.todotask.entity.ToDoListRepository
import com.example.todotask.entity.ToDoItem
import io.reactivex.Single

class GetTaskListUseCase(private val repository: ToDoListRepository) {
    fun execute(): Single<List<ToDoItem>> = repository.allTasks()
}
