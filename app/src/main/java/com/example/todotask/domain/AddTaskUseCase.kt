package com.example.todotask.domain

import com.example.todotask.entity.ToDoListRepository
import com.example.todotask.entity.ToDoItem
import io.reactivex.Completable

class AddTaskUseCase(private val repository: ToDoListRepository) {
    fun execute(item: ToDoItem): Completable = repository.addTaskToBase(item)
}