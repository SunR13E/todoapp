package com.example.todotask.domain

import com.example.todotask.entity.ToDoListRepository
import com.example.todotask.entity.ToDoItem
import io.reactivex.Completable

class UpdateTaskUseCase(private val repository: ToDoListRepository) {
    fun execute(toDoItem: ToDoItem): Completable = repository.updateTask(toDoItem)
}