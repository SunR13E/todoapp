package com.example.todotask.entity

import io.reactivex.Completable
import io.reactivex.Single

class ToDoListRepository(private val toDoListDao: ToDoListDao) {

    fun allTasks(): Single<List<ToDoItem>>{
        return toDoListDao.loadTask().firstOrError()
    }

    fun addTaskToBase(toDoItem: ToDoItem): Completable{
        return toDoListDao.addTask(toDoItem)
    }

    fun deleteTaskFromBase(toDoItem: ToDoItem): Completable{
        return toDoListDao.deleteTask(toDoItem)
    }

    fun updateTask(toDoItem: ToDoItem): Completable{
        return toDoListDao.updateTask(toDoItem)
    }

}