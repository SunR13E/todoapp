package com.example.todotask.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.todotask.entity.ToDoItem.Companion.NAME

@Entity(tableName = NAME)
data class ToDoItem (
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "taskNumber")
    val taskNumber: Int? = null,

    @ColumnInfo(name = "taskName")
    val taskName: String,

    @ColumnInfo(name = "taskInfo")
    val taskType: Int
){

    companion object{
        const val NAME = "to_do_list_name"
    }
}