package com.example.todotask.entity

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface ToDoListDao {

    @Query("SELECT * FROM to_do_list_name ORDER BY taskInfo DESC")
    fun loadTask(): Flowable<List<ToDoItem>>

    @Insert(entity = ToDoItem::class, onConflict = OnConflictStrategy.REPLACE)
    fun addTask(toDoEntity: ToDoItem): Completable

    @Delete(entity = ToDoItem::class)
    fun deleteTask(toDoEntity: ToDoItem): Completable

    @Update(entity = ToDoItem::class, onConflict = OnConflictStrategy.IGNORE)
    fun updateTask(toDoEntity: ToDoItem): Completable

}